# Les outils

## Intention

Une suite de logiciels sera utilisée dans ce module afin de créer un sentiment d'immersion et aussi pour sortir de l'habitude "support de cours" et présentation pléniaire.

## Gestion du travail

**Framework Scrum**

![iceScrum](img/iceScrum.PNG)

Nous gérerons à travers IceScrum :
* le backlog
* la vélocité du groupe
* l'évaluation (tests d'acceptation)
* le travail collaboratif

[Ice Scrum](icescrum.org)

**Wiki - Bitbucket**

![BitBucket](img/bitBucket.PNG)

Le contenu du cours sera hébergé sur un wiki configuré sur BitBucket.

**Google Suite - Création de contenu**

Dossier partagé pour le module (//TODO dev gmail.com adresses)

