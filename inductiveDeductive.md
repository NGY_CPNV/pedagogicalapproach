# Approche pédagogique

## Le raisonnement déductif ou inductif

Durant ce module, quatre approches seront favorisés:

* une approche empirique

    *L'empirisme est la doctrine selon laquelle toute connaissance découle de l'expérience.
    L'empirisme s'oppose donc au rationalisme et à l'idéalisme.*

    ![Emprisime](img/learnBicyle.PNG)

* une approche coopérative

    ![Cooperation Collaboration](img/cooperationCollaboration.PNG)

* une approche inductive

![Déductif et inductif](img/inductiveDeductive.PNG)

* une approche itérative

[![Itération](img/iteration.PNG)](https://docs.google.com/presentation/d/1TYO_LSGo8cgY7Tk2k2PeviGtuNNzbYmzUBuRFHqYu1M/edit?usp=sharing)


### Sources
https://www.espacefrancais.com/les-differents-modes-de-raisonnement/
http://outils-reseaux.org/ContenuCooperationCollaboration
https://www.planetepapas.com/
